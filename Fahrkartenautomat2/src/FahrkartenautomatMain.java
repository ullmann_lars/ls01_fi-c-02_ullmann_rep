import java.util.Scanner;

class FahrkartenautomatMain
{
    public static void main(String[] args)
    {
    	Scanner tastatur = new Scanner(System.in);

    	double eingezahlterGesamtbetrag;
    	double eingeworfeneM�nze;
    	double r�ckgabebetrag;
    	double eingegebenerBetrag;
    	double neuerzuZahlenderBetrag;

    	neuerzuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);
    	r�ckgabebetrag = fahrkartenBezahlen(tastatur, neuerzuZahlenderBetrag);
    	fahrkartenAusgabe();
    	r�ckgeldausgabe(r�ckgabebetrag);

    } // schluss main
    public static double fahrkartenbestellungErfassen (Scanner tastatur)
    {
    	double zuZahlenderBetrag;
    	double anzahlFahrkarten;
    	double neuerzuZahlenderBetrag;
    	System.out.print("Zu zahlender Betrag (EURO): ");
        zuZahlenderBetrag = tastatur.nextDouble();
        System.out.print("Anzahl Fahrkarten: ");
        anzahlFahrkarten = tastatur.nextDouble();
        neuerzuZahlenderBetrag = zuZahlenderBetrag * anzahlFahrkarten;
        return neuerzuZahlenderBetrag;
    } //schluss bestelleungerfassen
    public static double fahrkartenBezahlen (Scanner tastatur, double neuerzuZahlenderBetrag)
    {
    	double eingeworfeneM�nze;
    	double r�ckgabebetrag;
    	r�ckgabebetrag = 0.0;
         while(r�ckgabebetrag < neuerzuZahlenderBetrag)
         {
      	   System.out.printf("%s %.2f %s\n", "Noch zu zahlen: ", (neuerzuZahlenderBetrag - r�ckgabebetrag), "�");
      	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
      	   eingeworfeneM�nze = tastatur.nextDouble();
           r�ckgabebetrag += eingeworfeneM�nze;
         }
         return r�ckgabebetrag;
    } // schluss fahrkartenbezahlen
    public static void fahrkartenAusgabe () {
    	  System.out.println("\nFahrschein wird ausgegeben");
          for (int i = 0; i < 8; i++)
          {
             System.out.print("=");
             try {
   			Thread.sleep(250);
   		} catch (InterruptedException e) {
   			// TODO Auto-generated catch block
   			e.printStackTrace();
   		}
          }
          System.out.println("\n\n");
    } //schluss fahrkartenausgabe
    public static void r�ckgeldausgabe(double r�ckgabebetrag) {

    	if(r�ckgabebetrag > 0.0)
        {
     	   System.out.printf("%s %.2f %s\n", "Der R�ckgabebetrag in H�he von ", r�ckgabebetrag, " EURO");
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
         	  System.out.println("2 EURO");
 	          r�ckgabebetrag -= 2.0;
            }
            while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
         	  System.out.println("1 EURO");
 	          r�ckgabebetrag -= 1.0;
            }
            while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
         	  System.out.println("50 CENT");
 	          r�ckgabebetrag -= 0.5;
            }
            while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
         	  System.out.println("20 CENT");
  	          r�ckgabebetrag -= 0.2;
            }
            while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
         	  System.out.println("10 CENT");
 	          r�ckgabebetrag -= 0.1;
            }
            while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
         	  System.out.println("5 CENT");
  	          r�ckgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir w�nschen Ihnen eine gute Fahrt.");
    } //schluss r�ckgeldausgabe
} // ende